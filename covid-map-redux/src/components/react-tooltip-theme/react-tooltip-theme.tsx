import ReactTooltip from "react-tooltip"

const currentTheme = window.matchMedia("(prefers-color-scheme: light)").matches

interface Props {
	content: any
}

const ReactTooltipThemed = ({ content }: Props) => {
	return (
		<ReactTooltip type={currentTheme ? "dark" : "light"}>
			{content}
		</ReactTooltip>
	)
}
export default ReactTooltipThemed
