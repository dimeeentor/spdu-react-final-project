import "../../styles/no-match.scss"

import { Button } from "@material-ui/core"
import { Link } from "react-router-dom"
import { memo } from "react"

const NoMatch = () => {
	return (
		<div id="page-not-found">
			<h1>Page not found</h1>
			<Button
				variant="outlined"
				style={{
					border: "1px solid silver",
				}}
			>
				<Link to={"/"} id="back-to-map">
					Back to Map
				</Link>
			</Button>
		</div>
	)
}

export default memo(NoMatch)
