import "../../styles/header.scss"

import { memo, ReactChild } from "react"

interface Props {
	children?: ReactChild | ReactChild[]
}

const Header = ({ children }: Props) => {
	return (
		<header>
			<section className="global-info">{children}</section>
		</header>
	)
}

export default memo(Header)
