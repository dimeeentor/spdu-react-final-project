import { memo } from "react"
import { GlobalProps } from "../../interfaces/GlobalProps"
import Rounded from "../../modules/map-chart-info/round-number"

interface Props {
	statistics: GlobalProps
}

const StatisticElement = ({ statistics }: Props): any =>
	Object.entries(statistics).map(([statistic, value]) => (
		<p key={statistic}>
			{statistic}: {Rounded(value)}
		</p>
	))

export default memo(StatisticElement)
