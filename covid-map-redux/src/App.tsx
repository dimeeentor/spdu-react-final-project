import "./styles/App.scss"

import { useEffect, useState } from "react"
import { loadCovidSummaryThunk } from "./modules/services/actions/loadCovidSummaryThunk"

import MapChartInfo from "./modules/map-chart-info/map-chart-info"
import Sidebar from "./modules/map-chart-info/components/sidebar/sidebar"
import { useAppDispatch, useAppSelector } from "./store/hooks"
import Header from "./components/header/header"
import StatisticElement from "./components/statistic-element/statistic-element"
import CountryList from "./modules/map-chart-info/components/country-list/country-list"
import InfoBox from "./modules/map-chart-info/components/info-box/info-box"
import ReactTooltipThemed from "./components/react-tooltip-theme/react-tooltip-theme"

function App() {
	const dispatch = useAppDispatch()
	useEffect(() => {
		dispatch(loadCovidSummaryThunk())
	}, [dispatch])

	const {
		NewConfirmed,
		NewDeaths,
		NewRecovered,
		TotalConfirmed,
		TotalDeaths,
		TotalRecovered,
	} = useAppSelector((state) => state.covidSummary.Global)

	const [content, setContent] = useState("")

	return (
		<div className="App">
			<Header>
				<StatisticElement
					statistics={{
						NewConfirmed,
						NewDeaths,
						NewRecovered,
						TotalConfirmed,
						TotalDeaths,
						TotalRecovered,
					}}
				/>
			</Header>

			<Sidebar>
				<>
					<CountryList />
				</>
			</Sidebar>

			<MapChartInfo className="map" setTooltipContent={setContent} />
			<ReactTooltipThemed content={content} />
			<InfoBox />
		</div>
	)
}

export default App
