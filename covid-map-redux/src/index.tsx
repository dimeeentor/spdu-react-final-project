import "./styles/index.scss"
import App from "./App"
import React, { lazy, Suspense } from "react"
import ReactDOM from "react-dom"
import { store } from "./store/store"
import { Provider } from "react-redux"
import * as serviceWorker from "./serviceWorker"
import { BrowserRouter, Route, Routes } from "react-router-dom"
import Loader from "./components/loader/loader"

const CountryInfo = lazy(
	() => import("./modules/map-chart-info/components/country-info/country-info")
)
const NoMatch = lazy(() => import("./components/no-match/no-match"))

ReactDOM.render(
	<React.StrictMode>
		<Provider store={store}>
			<BrowserRouter>
				<Routes>
					<Route index element={<App />} />
					<Route
						path="/country-info/:countryName"
						element={
							<Suspense fallback={<Loader />}>
								<CountryInfo />
							</Suspense>
						}
					/>
					<Route
						path="*"
						element={
							<Suspense fallback={<Loader />}>
								<NoMatch />
							</Suspense>
						}
					/>
				</Routes>
			</BrowserRouter>
		</Provider>
	</React.StrictMode>,
	document.getElementById("root")
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
