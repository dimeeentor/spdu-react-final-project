import { combineReducers } from "@reduxjs/toolkit"
import covidSummary from "../modules/services/reducers"

export const rootReducer = combineReducers({
	covidSummary
})
