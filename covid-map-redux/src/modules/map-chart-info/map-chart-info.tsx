import { memo } from "react"

import {
	ZoomableGroup,
	ComposableMap,
	Geographies,
	Geography,
} from "react-simple-maps"

import { useAppSelector } from "../../store/hooks"
import { countryAdapter } from "../services/reducers"
import { Country } from "../../interfaces/Country"
import { CountryToColor } from "./country-to-color"
import ReactTooltipInfo from "./react-tooltip-info"

const geoUrl =
	"https://raw.githubusercontent.com/zcreativelabs/react-simple-maps/master/topojson-maps/world-110m.json"

const MapChartInfo = ({ setTooltipContent }: any) => {
	const countries = useAppSelector((state) =>
		countryAdapter.getSelectors().selectAll(state.covidSummary.Countries)
	)

	const summaryCountries = new Map(
		countries.map((app) => [app.CountryCode, app])
	)

	return (
		<>
			<ComposableMap data-tip="" projectionConfig={{ scale: 200 }}>
				<ZoomableGroup>
					<Geographies geography={geoUrl}>
						{({ geographies }) =>
							geographies.map((geo) => {
								const { NAME, POP_EST, ISO_A2 } = geo.properties

								const currentCountry: Country = summaryCountries.get(ISO_A2)!
								const currentColor = CountryToColor(
									currentCountry?.TotalConfirmed
								)

								return (
									<Geography
										key={geo.rsmKey}
										geography={geo}
										onClick={() =>
											window.open(`/country-info/${NAME}`, "_self")
										}
										onMouseEnter={() =>
											setTooltipContent(ReactTooltipInfo({ NAME, POP_EST, currentCountry }))
										}
										onMouseLeave={() => setTooltipContent("")}
										style={{
											default: {
												fill: currentColor,
												stroke: "#131313",
												strokeWidth: 0.2,
											},
											hover: {
												fill: "#404040",
												outline: "none",
												cursor: "pointer",
											},
											pressed: {
												fill: "#E42",
												outline: "none",
											},
										}}
									/>
								)
							})
						}
					</Geographies>
				</ZoomableGroup>
			</ComposableMap>
		</>
	)
}

export default memo(MapChartInfo)
