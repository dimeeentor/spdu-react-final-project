import "../../../../styles/sidebar.scss"

import { Button } from "@material-ui/core"
import {
	memo,
	ReactChild,
	useCallback,
	useEffect,
	useMemo,
	useState,
} from "react"
import { useAppDispatch, useAppSelector } from "../../../../store/hooks"

import {
	setAmountOfCountries,
	setSearchCountry,
	setSortByConfirmed,
} from "../../../services/reducers"
import useDebounce from "../../../../hooks/useDebounce/useDebounce"

interface Props {
	children?: ReactChild | ReactChild[]
}

const Sidebar = ({ children }: Props) => {
	const dispatch = useAppDispatch()

	const sortByConfirmed = useAppSelector(
		(state) => state.covidSummary.sortByConfirmed
	)
	const amountOfCountries = useAppSelector(
		(state) => state.covidSummary.amountOfCountries
	)

	const [searchTerm, setSearchTerm] = useState("")
	const onChange = useCallback(
		(e) => {
			dispatch(setAmountOfCountries(e.target.value))
		},
		[dispatch]
	)

	const style = useMemo(() => {
		return {
			border: "1px solid silver",
		}
	}, [])

	const debouncedSearchTerm = useDebounce(searchTerm, 500)
	useEffect(() => {
		if (debouncedSearchTerm) {
			dispatch(setSearchCountry(searchTerm))
		}
	}, [debouncedSearchTerm, dispatch, searchTerm])

	return (
		<section className="all-countries">
			<div className="search-area">
				<input
					placeholder="Search for countries"
					id="search-country"
					autoComplete="off"
					onChange={(e) => setSearchTerm(e.target.value)}
				/>

				<Button
					variant="outlined"
					onClick={() => dispatch(setSortByConfirmed(!sortByConfirmed))}
					style={style}
				>
					{sortByConfirmed ? "Unsort" : "Sort"} By New Confirmed
				</Button>

				<label>
					<span>Amount of countries to show: {amountOfCountries}</span>
					<input
						type="range"
						name="amount-of-countries"
						step="1"
						max="10"
						min="1"
						id="amount-of-countries"
						defaultValue="5"
						onChange={onChange}
					/>
				</label>
			</div>
			{children}
		</section>
	)
}

export default memo(Sidebar)
