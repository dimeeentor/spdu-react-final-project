import "../../../../styles/info-box.scss"

import { memo } from "react"
import { colors } from "../../country-to-color"

const InfoBox = () => {
	return (
		<section className="info-box">
			<div className="info-box__total">
				<h3>Total Confirmed:</h3>
				<p>&gt;50M</p>
				<p>&gt;40M</p>
				<p>&gt;30M</p>
				<p>&gt;20M</p>
				<p>&gt;10M</p>
				<p>&gt;5M</p>
				<p>&gt;1M</p>
				<p>&gt;100K</p>
				<p>&gt;5K</p>
				<p>No information</p>
			</div>
      
			<div className="info-box__colors">
				{colors.map((color) => (
					<div
						style={{
							background: color,
							height: "15px",
							width: "15px",
							marginTop: "5px",
						}}
						key={color}
					/>
				))}
			</div>
		</section>
	)
}

export default memo(InfoBox)
