import "../../../../styles/country-list.scss"

import { Country } from "../../../../interfaces/Country"
import { useAppSelector } from "../../../../store/hooks"
import { memo } from "react"
import CountryCard from "./country-card"

const CountryList = () => {
	const countries = useAppSelector(
		(state) => state.covidSummary.Countries.entities
	)
	const sortByConfirmed = useAppSelector(
		(state) => state.covidSummary.sortByConfirmed
	)
	const searchCountry = useAppSelector(
		(state) => state.covidSummary.searchCountry
	)
	const amountOfCountries = useAppSelector(
		(state) => state.covidSummary.amountOfCountries
	)

	const filterList = (app: Country | undefined) => {
		if (!searchCountry) {
			return app
		} else if (
			app?.Country.toLowerCase().includes(searchCountry.toLowerCase())
		) {
			return app
		}
	}

	if (sortByConfirmed) {
		return (
			<>
				{Object.values(countries)
					.sort(
						(a, b) => (a?.NewConfirmed as number) - (b?.NewConfirmed as number)
					)
					.reverse()
					.filter((app) => filterList(app))
					.map((app) => (
						<CountryCard
							Country={app?.Country as string}
							NewConfirmed={app?.NewConfirmed as number}
							NewDeaths={app?.NewDeaths as number}
							NewRecovered={app?.NewRecovered as number}
							TotalConfirmed={app?.TotalConfirmed as number}
							TotalDeaths={app?.TotalDeaths as number}
							TotalRecovered={app?.TotalRecovered as number}
							key={app?.Country as string}
						/>
					))
					.slice(0, amountOfCountries)}
			</>
		)
	} else {
		return (
			<>
				{Object.values(countries)
					.filter((app) => filterList(app))
					.map((app) => (
						<CountryCard
							Country={app?.Country as string}
							NewConfirmed={app?.NewConfirmed as number}
							NewDeaths={app?.NewDeaths as number}
							NewRecovered={app?.NewRecovered as number}
							TotalConfirmed={app?.TotalConfirmed as number}
							TotalDeaths={app?.TotalDeaths as number}
							TotalRecovered={app?.TotalRecovered as number}
							key={app?.Country as string}
						/>
					))
					.slice(0, amountOfCountries)}
			</>
		)
	}
}

export default memo(CountryList)
