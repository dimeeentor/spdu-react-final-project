import { Link } from "react-router-dom"
import { Country } from "../../../../interfaces/Country"
import StatisticElement from "../../../../components/statistic-element/statistic-element"
import { useAppDispatch } from "../../../../store/hooks"
import { setSearchCountry } from "../../../services/reducers"
import { memo, useCallback } from "react"

const CountryCard = ({
	Country,
	NewConfirmed,
	NewDeaths,
	NewRecovered,
	TotalConfirmed,
	TotalDeaths,
	TotalRecovered,
}: Country) => {
	const dispatch = useAppDispatch()
	const onClick = useCallback(() => dispatch(setSearchCountry("")), [dispatch])
  
	return (
		<div className="country-name" key={Country}>
			<Link to={`/country-info/${Country}`} onClick={onClick}>
				<div className="text">{Country}</div>
				<StatisticElement
					statistics={{
						NewConfirmed,
						NewDeaths,
						NewRecovered,
						TotalConfirmed,
						TotalDeaths,
						TotalRecovered,
					}}
				/>
			</Link>
		</div>
	)
}

export default memo(CountryCard)
