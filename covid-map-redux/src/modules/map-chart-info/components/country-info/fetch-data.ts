import { useAppSelector } from "../../../../store/hooks"

const FetchData = () => {
	const currentCountry = useAppSelector(
		(state) => state.covidSummary.currentCountry
	)

	const fetchData: [{}] = [
		[{ type: "date", label: "Day" }, "Confirmed", "Deaths", "Recovered"],
	]

	Object.values(currentCountry).map((app) => {
		const currentDay = []
		currentDay.push(new Date(app.Date))
		currentDay.push(app.Confirmed)
		currentDay.push(app.Deaths)
		currentDay.push(app.Recovered)
		return fetchData.push(currentDay)
	})

	return fetchData
}

export default FetchData
