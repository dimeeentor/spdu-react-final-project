import "../../../../styles/country-info.scss"
import { Link, useParams } from "react-router-dom"

import { lazy, memo, useCallback, useEffect, useMemo, useState } from "react"
import { useAppDispatch } from "../../../../store/hooks"
import { Button } from "@material-ui/core"
import { loadCountryStatsThunk } from "../../../services/actions/loadCountryStatsThunk"
import FetchData from "./fetch-data"

const Chart = lazy(() => import("react-google-charts"))

const CountryInfo = () => {
	const { countryName } = useParams()
	const dispatch = useAppDispatch()

	const [fromDate, setFromDate] = useState("")
	const [toDate, setToDate] = useState("")
	const onBlurFrom = useCallback((e) => setFromDate(e.target.value), [])
	const onBlurTo = useCallback((e) => setToDate(e.target.value), [])

	const options = useMemo(() => {
		return {
			chart: {
				title: `${countryName}`,
				subtitle: `Detailed statistics in ${countryName} about confirmed, death and recoverd cases.`,
			},
		}
	}, [countryName])

	useEffect(() => {
		dispatch(
			loadCountryStatsThunk({
				country: countryName as string,
				from: fromDate,
				to: toDate,
			})
		)
	}, [countryName, dispatch, fromDate, toDate])

	const fetchData = FetchData()

	return (
		<main className="country-info">
			<Button variant="outlined">
				<Link to={"/"}>Back to Map</Link>
			</Button>

			<section className="country-info__date-picker">
				<label>
					<span>From: </span>
					<input type="date" name="from" onBlur={onBlurFrom} />
				</label>

				<label>
					<span>To: </span>
					<input type="date" name="to" onBlur={onBlurTo} />
				</label>
			</section>

			<Chart
				chartType="Line"
				width="100%"
				height="calc(100vh - 250px)"
				data={fetchData}
				options={options}
			/>
		</main>
	)
}

export default memo(CountryInfo)
