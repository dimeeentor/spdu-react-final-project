import { Country } from "../../interfaces/Country"
import Rounded from "./round-number"

interface Props {
	NAME: string
	POP_EST: number
	currentCountry: Country
}

const ReactTooltipInfo = ({ NAME, POP_EST, currentCountry }: Props) =>
	`${NAME} — Population: ${Rounded(POP_EST)} New Confirmed: ${Rounded(
		currentCountry?.NewConfirmed
	)} Total Confirmed: ${Rounded(
		currentCountry?.TotalConfirmed
	)} New Deaths: ${Rounded(currentCountry?.NewDeaths)} Total Deaths: ${Rounded(
		currentCountry?.TotalDeaths
	)}`

export default ReactTooltipInfo
