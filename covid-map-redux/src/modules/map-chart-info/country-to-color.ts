export const colors = [
	"#590d22",
	"#800f2f",
	"#a4133c",
	"#c9184a",
	"#ff4d6d",
	"#ff758f",
	"#ff8fa3",
	"#ffb3c1",
	"#ffccd5",
	"#fff0f9",
]

export const CountryToColor = (totalConfirmed: number) => {
	if (totalConfirmed > 50000000) {
		return colors[1]
	} else if (totalConfirmed > 40000000) {
		return colors[2]
	} else if (totalConfirmed > 30000000) {
		return colors[3]
	} else if (totalConfirmed > 20000000) {
		return colors[4]
	} else if (totalConfirmed > 10000000) {
		return colors[5]
	} else if (totalConfirmed > 1000000) {
		return colors[6]
	} else if (totalConfirmed > 100000) {
		return colors[7]
	} else if (totalConfirmed > 5000) {
		return colors[8]
	}
	return colors[9]
}