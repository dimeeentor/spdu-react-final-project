import {
	createEntityAdapter,
	createSlice,
	PayloadAction,
} from "@reduxjs/toolkit"
import { Country } from "../../interfaces/Country"
import { GlobalProps } from "../../interfaces/GlobalProps"
import { loadCountryStatsThunk } from "./actions/loadCountryStatsThunk"
import { loadCovidSummaryThunk } from "./actions/loadCovidSummaryThunk"
import { CountryStats } from "../../interfaces/CountryStats"

export const countryAdapter = createEntityAdapter<Country>({
	selectId: (country) => country.Country,
	sortComparer: false,
})

const defaultState = {
	Countries: countryAdapter.getInitialState(),
	Global: {} as GlobalProps,
	currentCountry: {} as CountryStats[],
	searchCountry: "",
	sortByConfirmed: false,
	amountOfCountries: 5,
}

const covidSummary = createSlice({
	name: "covid-summary",
	initialState: defaultState,
	reducers: {
		setSearchCountry(state, action: PayloadAction<string>) {
			state.searchCountry = action.payload
		},
		setSortByConfirmed(state, action: PayloadAction<boolean>) {
			state.sortByConfirmed = action.payload
		},
		setAmountOfCountries(state, action: PayloadAction<number>) {
			state.amountOfCountries = action.payload
		},
	},
	extraReducers: (builder) => {
		builder
			.addCase(
				loadCovidSummaryThunk.fulfilled,
				(
					state,
					{
						payload: { Countries, Global },
					}: PayloadAction<{ Countries: Country[]; Global: GlobalProps }>
				) => {
					countryAdapter.setAll(state.Countries, Countries)
					state.Global = Global
				}
			)
			.addCase(loadCountryStatsThunk.fulfilled, (state, action) => {
				state.currentCountry = action.payload
			})
	},
})

export const { setSearchCountry, setSortByConfirmed, setAmountOfCountries } =
	covidSummary.actions
export default covidSummary.reducer
