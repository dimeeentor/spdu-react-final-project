import { createAsyncThunk } from "@reduxjs/toolkit"
import { CountryStats } from "../../../interfaces/CountryStats"

export const loadCountryStatsThunk = createAsyncThunk<
	CountryStats[],
	{ country: string; from: string; to: string }
>("load-country-stats", (info) =>
	fetch(
		`https://api.covid19api.com/country/${info.country}?from=${info.from}&to=${info.to}`
	).then((result) => result.json().then((data) => data))
)
