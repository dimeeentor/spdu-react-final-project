import { createAsyncThunk } from "@reduxjs/toolkit"
import { Country } from "../../../interfaces/Country"
import { GlobalProps } from "../../../interfaces/GlobalProps"

export const loadCovidSummaryThunk = createAsyncThunk<{
	Countries: Country[]
	Global: GlobalProps
}>("load-covid-summary", () =>
	fetch("https://api.covid19api.com/summary").then((result) =>
		result.json().then((data) => data)
	)
)
